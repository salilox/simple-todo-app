import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

new Vue({
  el: '#app' ,
  data: {
    message :'hello world'
  },
  render: h => h(App),
}).$mount('#app')

